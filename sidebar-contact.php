<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Quark
 * @since Quark 1.0
 */
?>
                <?php
			do_action( 'before_sidebar' );

                        if( is_page_template( 'page-templates/contact-dark-sidebar.php' ) && is_active_sidebar( 'contact-sidebar' ) ){
                                $sidebar = 'contact-sidebar';
                        }
                        else {                                        
                                $sidebar = 'sidebar-main';  
                        }
                ?>
                        
	                <div class="col contact-black-sidebar">
		        <div id="secondary" class="widget-area" role="complementary">
                <?php
                        dynamic_sidebar( $sidebar );
		?>

		        </div> <!-- /#secondary.widget-area -->
                        </div> <!-- /.col.contact-black-sidebar -->



                <?php
			do_action( 'before_sidebar' );

                        if( is_page_template( 'page-templates/contact.php' ) && is_active_sidebar( 'contact-sidebar' ) ){
                                $sidebar = 'contact-sidebar';
                        }
                        else {                                        
                                $sidebar = 'sidebar-main';  
                        }
                ?>
                        
	                <div class="col grid_4_of_12">
                        <div id="secondary" class="widget-area" role="complementary">

                <?php
                        dynamic_sidebar( $sidebar );
		?>

		        </div> <!-- /#secondary.widget-area -->
                        </div> <!-- /.col.grid_4_of_12 -->
