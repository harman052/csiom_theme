<?php
/**
 * Template Name: Portfolio Template
 *
 * Description: Displays a full-width beautiful portfolio.
 *
 * @package Quark
 * @since Quark 1.0
 */
get_header(); ?>
	
	<div id="portfolioBannerContainer">
		<div class="banner-portfolio row">
			<?php
				// Count how many banner sidebars are active so we can work out how many containers we need
				$bannerSidebars = 0;
				for ( $x = 1; $x <= 2; $x++ ) {
					if ( is_active_sidebar( 'portfolio-template' . $x ) ) {
						$bannerSidebars++;
					}
				}

				// If there's one or more one active sidebars, create a row and add them
				if ( $bannerSidebars > 0 ) { 
					
					// Work out the container class name based on the number of active banner sidebars
					$containerClass = "grid_" . 12 / $bannerSidebars . "_of_12";

					// Display the active banner sidebars
					for ( $x = 1; $x <= 2; $x++ ) {
						if ( is_active_sidebar( 'portfolio-template'. $x ) ) { ?>
							<div class="col <?php echo $containerClass?>">
								<div class="widget-area" role="complementary">
									<?php dynamic_sidebar( 'portfolio-template'. $x ); ?>
								</div> <!-- /.widget-area -->
							</div> <!-- /.col.<?php echo $containerClass?> -->
						<?php }
					} ?>

                                <?php } ?>
		
		</div> <!-- /.banner.row -->
	</div> <!-- /#bannercontainer -->

	<div id="maincontentcontainer">
				
	<?php
                project_categories();
		echo"<div class=\"main\">
			<ul id=\"og-grid\" class=\"og-grid\">";
                echo "</ul></div>"
	?>
                
        <script type="text/javascript">
		$( function() {
			Grid.init();
		});
		
		$(document).ready(function() {
                });
	</script>

<?php $gridjs_path = get_template_directory_uri() . '/js/grid.min.js'; ?>
                        
                <script type="text/javascript">
                        var gridjs_path = "<?php echo $gridjs_path;  ?>";
                        $.post( ajax_object.ajaxurl, {'action': 'port1', 'data':   'all'}, 
                        function(response) {
                                $('#all').css({"background-color":"#B3B3B3", "color":"white"});
				$( "div.main" ).html(response);
	$.getScript(gridjs_path, function() {
		Grid.init();
		});
	});
</script>

	<?php get_footer(); ?>
