<?php
/**
 * Template Name: Blog template with banner & left sidebar
 *
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Quark
 * @since Quark 1.0
 */

get_header(); ?>
	<div id="leftSidebarBannerContainer">
		<div class="banner row">
			<?php if ( is_front_page() ) {
				// Count how many banner sidebars are active so we can work out how many containers we need
				$bannerSidebars = 0;
				for ( $x=1; $x<=2; $x++ ) {
					if ( is_active_sidebar( 'left-sidebar-banner' . $x ) ) {
						$bannerSidebars++;
					}
				}

				// If there's one or more one active sidebars, create a row and add them
				if ( $bannerSidebars > 0 ) { ?>
					<?php
					// Work out the container class name based on the number of active banner sidebars
					$containerClass = "grid_" . 12 / $bannerSidebars . "_of_12";

					// Display the active banner sidebars
					for ( $x=1; $x<=2; $x++ ) {
						if ( is_active_sidebar( 'left-sidebar-banner'. $x ) ) { ?>
							<div class="col <?php echo $containerClass?>">
								<div class="widget-area" role="complementary">
									<?php dynamic_sidebar( 'left-sidebar-banner'. $x ); ?>
								</div> <!-- /.widget-area -->
							</div> <!-- /.col.<?php echo $containerClass?> -->
						<?php }
					} ?>

				<?php }
			} ?>
		</div> <!-- /.banner.row -->
	</div> <!-- /#leftSidebarPageContainer -->
	<div id="primary" class="site-content row" role="main">
	<?php get_sidebar(); ?>
		<div class="col grid_8_of_12">
                        <?php
                                $args = array( 'posts_per_page' => 10, 'order'=> '', 'orderby' => '' );
                                $postslist = get_posts( $args );
                                foreach ( $postslist as $post ) :
                                setup_postdata( $post ); ?> 
                <div>
                        <?php get_template_part( 'content',  get_post_format() ); ?>
                </div>
                <?php
                        endforeach; 
                        wp_reset_postdata();
                ?>
	</div> <!-- /.col.grid_8_of_12 -->

	</div> <!-- /#primary.site-content.row -->

<?php get_footer(); ?>
