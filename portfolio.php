<?php
function my_custom_post_portfolio() {
  $labels = array(
    'name'               => _x( 'Projects', 'post type general name' ),
    'singular_name'      => _x( 'Project', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'project' ),
    'add_new_item'       => __( 'Add New Project' ),
    'edit_item'          => __( 'Edit Project' ),
    'new_item'           => __( 'New Project' ),
    'all_items'          => __( 'All Projects' ),
    'view_item'          => __( 'View Projects' ),
    'search_items'       => __( 'Search Projects' ),
    'not_found'          => __( 'No projects found' ),
    'not_found_in_trash' => __( 'No projects found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Portfolio'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds details of projects completed by company',
    'public'        => true,
    'menu_position' => 60,
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
    'has_archive'   => true,
  );
  register_post_type( 'cs_portfolio', $args ); 
}
add_action( 'init', 'my_custom_post_portfolio' );

function my_taxonomies_project() {
  $labels = array(
    'name'              => _x( 'Project Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Project Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Project Categories' ),
    'all_items'         => __( 'All Project Categories' ),
    'parent_item'       => __( 'Parent Project Category' ),
    'parent_item_colon' => __( 'Parent Project Category:' ),
    'edit_item'         => __( 'Edit Project Category' ), 
    'update_item'       => __( 'Update Project Category' ),
    'add_new_item'      => __( 'Add New Project Category' ),
    'new_item_name'     => __( 'New Project Category' ),
    'menu_name'         => __( 'Project Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'project_category', 'cs_portfolio', $args );
}
add_action( 'init', 'my_taxonomies_project', 0 );

/**
 * Meta box for featured image heading
 */

add_action( 'add_meta_boxes', 'project_image_heading_box' );
function project_image_heading_box() {
    add_meta_box( 
        'project_image_heading_box',
        __( 'Image Heading', 'portfolio' ),
        'project_image_heading_box_content',
        'cs_portfolio',
        'normal',
        'high'
    );
}

function project_image_heading_box_content( $post ) {
  $project_image_heading = get_post_meta( get_the_ID(), 'project_image_heading', true );
  wp_nonce_field( plugin_basename( __FILE__ ), 'project_image_heading_box_content_nonce' );
  echo '<label for="project_image_heading"></label>';
  echo '<input type="text" size="50%" id="project_image_heading" name="project_image_heading" value="'.$project_image_heading.'" placeholder="Caption text for featured project image" />';
}

add_action( 'save_post', 'project_image_heading_box_save' );
function project_image_heading_box_save( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
  return;

  if ( !wp_verify_nonce( $_POST['project_image_heading_box_content_nonce'], plugin_basename( __FILE__ ) ) )
  return;

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }
  $project_image_heading = $_POST['project_image_heading'];
  update_post_meta( $post_id, 'project_image_heading', $project_image_heading );
}

/**
 * Featured image caption
 */

add_action( 'add_meta_boxes', 'image_caption_box' );
function image_caption_box() {
    add_meta_box( 
        'image_caption_box',
        __( 'Image Caption', 'portfolio' ),
        'image_caption_box_content',
        'cs_portfolio',
        'normal',
        'high'
    );
}

function image_caption_box_content( $post ) {
  $image_caption = get_post_meta( get_the_ID(), 'image_caption', true );
  wp_nonce_field( plugin_basename( __FILE__ ), 'image_caption_box_content_nonce' );
  echo '<label for="image_caption"></label>';
  echo '<input type="text" size="50%" id="image_caption" name="image_caption" value="'.$image_caption.'" placeholder="Caption text for featured project image" />';
}

add_action( 'save_post', 'image_caption_box_save' );
function image_caption_box_save( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
  return;

  if ( !wp_verify_nonce( $_POST['image_caption_box_content_nonce'], plugin_basename( __FILE__ ) ) )
  return;

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }
  $image_caption = $_POST['image_caption'];
  update_post_meta( $post_id, 'image_caption', $image_caption );
}



/**
 * Meta box for Project link
 */
add_action( 'add_meta_boxes', 'project_link_box' );
function project_link_box() {
    add_meta_box( 
        'project_link_box',
        __( 'Project Link', 'portfolio' ),
        'project_link_box_content',
        'cs_portfolio',
        'normal',
        'high'
    );
}

function project_link_box_content( $post ) {
  $project_link = get_post_meta( get_the_ID(), 'project_link', true );
  wp_nonce_field( plugin_basename( __FILE__ ), 'project_link_box_content_nonce' );
  echo '<label for="project_link"></label>';
  echo '<input type="text" size="50%" id="project_link" name="project_link" value="'.$project_link.'" placeholder="Fill the project link" />';
}

add_action( 'save_post', 'project_link_box_save' );
function project_link_box_save( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
  return;

  if ( !wp_verify_nonce( $_POST['project_link_box_content_nonce'], plugin_basename( __FILE__ ) ) )
  return;

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }
  $project_link = $_POST['project_link'];
  update_post_meta( $post_id, 'project_link', $project_link );
}



?>
