An easily customizable flat design theme for creating stunning and 
responsive WordPress websites. Equipped with inbuilt options to 
create ravishing and striking portfolio, it is suitable for people 
ranging from individuals to small & medium enterprises. 

It's backed by HTML5, CSS3, AJAX, JQuery and of course lots of love.

Use it freely for any purpose and don't forget to give your feedback 
and / or suggestions. 


Enjoy. :)
